import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions/postAction'
import { createBrief } from '../actions/postAction'
import PropTypes from 'prop-types'
import Select from 'react-select'


class BriefForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            comment: '',
            productId:''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]:event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        var pId;
        console.log(this.state.productId);
        switch (this.state.productId) {
            case 'Bag Box':
                    pId=1
                    break;
            case 'Oval Box':
                pId=2
                break;
            case 'Round Box':
                pId=3
                break;
            case 'Other Box':
                pId=4
                break;
            default:
                pId=0
                break;
        }
        const post = {
            title: this.state.title,
            comment: this.state.comment,

            productId: pId
        };


        this.props.createBrief(post);
    }

    componentDidMount() {
        this.props.fetchPosts();
    }


    render() {

        var options = this.props.posts.map(post => {
                return <option key = {post.id}>{post.name}</option>
            }

        );

        return (
            <div>
                <h1>Brief</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Title:
                        <input type="text" name="title"  value={this.state.title} onChange={this.handleChange} />
                    </label>
                    <br/>
                    <label>
                        Comment:
                        <input type="text" name="comment" value={this.state.comment} onChange={this.handleChange} />
                    </label>
                    <br/>
                    <label>
                        Pick the product:
                        <select  name="productId" value={this.state.productId} onChange={this.handleChange}>
                            {options}
                        </select>
                    </label>
                    <br/>
                    <input type="submit" value="Submit" />
                </form>
                <hr/>
            </div>
        );
    }
}

BriefForm.propTypes = {
    createBrief: PropTypes.func.isRequired,
    fetchPosts: PropTypes.func.isRequired,
    posts: PropTypes.array.isRequired,

}

const  mapStateToProps = state => ({
    posts:state.posts.items,

});

export default connect(mapStateToProps, {fetchPosts,createBrief})(BriefForm);

/**
 * Created by ramadepaul on 02/03/19.
 */
/**
 * Created by ramadepaul on 04/04/19.
 */
