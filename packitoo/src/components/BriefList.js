import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchBriefs } from '../actions/postAction'
import PropTypes from 'prop-types'
import Select from 'react-select'


class BriefList extends Component {

    componentDidMount() {
        this.props.fetchBriefs();
    }


    componentWillReceiveProps(nextProps) {
        if(nextProps.newPost) {
            this.props.briefs.unshift(nextProps.newPost);
        }
    }





    render() {
        const postItems = this.props.briefs.map(post => (
            <div key={post.id}>
                <h3>{post.title}</h3>
                <p>{post.comment}</p>
                <span>{post.productId}</span>
            </div>
        ));

        return (
            <div>

                <h1>Products</h1>
                {postItems}
            </div>
        );
    }
}

BriefList.propTypes = {
    fetchBriefs: PropTypes.func.isRequired,
    briefs: PropTypes.array.isRequired,
    newPost:PropTypes.object

}

const  mapStateToProps = state => ({
    briefs:state.posts.briefs,
    newPost: state.posts.item
});

export default connect(mapStateToProps, {fetchBriefs})(BriefList);


