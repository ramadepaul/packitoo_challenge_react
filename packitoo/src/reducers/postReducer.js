/**
 * Created by ramadepaul on 02/04/19.
 */
import { FETCH_POSTS, NEW_POST, FETCH_BRIEFS} from '../actions/types'

const initialState = {
    items: [],
    item:{},
    briefs: []
}

export default function (state = initialState, action) {
    switch (action.type)  {
        case FETCH_POSTS:
            return {
                ...state,
                items: action.payload
            };
        case NEW_POST:
            return {
                ...state,
                item: action.payload
            };
        case FETCH_BRIEFS:
            console.log("yes")
            return {
                ...state,
                briefs: action.payload
            };
        default:
            return state;
    }
}