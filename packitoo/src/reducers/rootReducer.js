/**
 * Created by ramadepaul on 02/03/19.
 */
import { combineReducers } from 'redux';
import simpleReducer from './simpleReducer';
import postReducer from './postReducer'

export default combineReducers({
    posts: postReducer
});