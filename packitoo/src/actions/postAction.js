/**
 * Created by ramadepaul on 02/04/19.
 */
import { FETCH_POSTS, NEW_POST, FETCH_BRIEFS} from './types'

export const fetchPosts = () => dispatch => {
        fetch('http://localhost:3000/products')
            .then(res => res.json())
            .then(posts => dispatch({
                type: FETCH_POSTS,
                payload: posts
            }));
};

export const fetchBriefs = () => dispatch => {
    console.log("fetching briefs")
    fetch('http://localhost:3000/briefs')
        .then(res => res.json())
        .then(briefs  => dispatch({
            type: FETCH_BRIEFS,
            payload: briefs
        }));
};

export const createBrief = (postData) => dispatch => {
    console.log('actioncalled');
    fetch('http://localhost:3000/briefs', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(postData)
    })
        .then(res => res.json())
        .then(post => dispatch({
            type: NEW_POST,
            payload: post
        }));
};