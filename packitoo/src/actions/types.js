/**
 * Created by ramadepaul on 02/04/19.
 */
export const FETCH_POSTS = 'FETCH_POSTS';
export const NEW_POST = 'NEW_POST';
export const FETCH_BRIEFS = 'FETCH_BRIEFS';